﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FourthLessonOTUS_6_
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectString = "Data Source = vpn.ecsogy.ru; Initial Catalog = OTUS_home_linq_6; User ID = OTUS_lessonsUser; Password = vfvfjeufh7320s$^&@!vfvfjeufh7320s$^&@!";
            DataClasses1DataContext db = new DataClasses1DataContext(connectString);

            string login = "loginsjk";
            string password = "passgj3";

            Console.WriteLine($"Provided credentials are login '{login}' password '{password}'\n");

            var user = (from item in db.Users
                        where item.login == login && item.password == password
                        select item).First();
            Console.WriteLine($"Found user\n" +
                $"User id: {user.id} " +
                $"{user.first_name.Trim()} " +
                $"{user.second_name.Trim()} " +
                $"Phone: {user.phone.Trim()} " +
                $"Passport number: {user.face_id} " +
                $"Registration date: {user.registration_date.Trim()}\n");

            var usersAccounts = from account in db.Accounts
                                join history in db.Histories on account.id equals history.account_id into history
                                where account.user_id == user.id
                                select new { account.id, account.opening_date, account.balance, history };
            Console.WriteLine($"\nFound {usersAccounts.Count()} accounts for user {user.first_name.Trim()} {user.second_name.Trim()}");
            foreach (var account in usersAccounts)
            {
                Console.WriteLine($"\nAccount id: {account.id} Balance: {account.balance} Opening date: {account.opening_date}");
                foreach (var historyItem in account.history)
                    Console.WriteLine($"Transaction id: {historyItem.id} " +
                        $"Date: {historyItem.date.Trim()} " +
                        $"Operation type: {historyItem.type.Trim()} " +
                        $"Transaction amount: {historyItem.sum}");
            }

            var refillAccountsHistory = from item in db.Histories
                                        where item.type == "refill"
                                        select item;
            Console.WriteLine("\nRefill transaction history");
            foreach (var refillHistoryItem in refillAccountsHistory)
            {
                var refillItemUser = (from account in db.Accounts
                                      join user2 in db.Users on account.user_id equals user2.id
                                      where account.id == refillHistoryItem.account_id
                                      select user2).First();
                Console.WriteLine($"id: {refillHistoryItem.id} User: {refillItemUser.first_name.Trim()} {refillItemUser.second_name.Trim()} Amount: {refillHistoryItem.sum} Date: {refillHistoryItem.date}");
            }

            int N = 100;
            var richAcccount = db.Accounts.Where(y => y.balance > N).Select(x => x.user_id).Distinct();
            Console.WriteLine("\nRich users are");
            foreach (var acc in richAcccount)
            {
                var richUser = db.Users.First(x => x.id == acc);
                Console.WriteLine($"User id: {richUser.id} Name: {richUser.first_name.Trim()} {richUser.second_name.Trim()}");
            }
            Console.WriteLine($"Their balace are more than '{N}' RUB");


            Console.ReadLine();
        }
    }
}
